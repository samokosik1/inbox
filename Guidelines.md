stages:
  - build

gpu-job:
  stage: build
  tags:
    -  saas-linux-medium-amd64-gpu-standard
  image: registry.gitlab.com/samokosik1/gpu:3
  variables:
    RUNNER_SCRIPT_TIMEOUT: 23h
    RUNNER_AFTER_SCRIPT_TIMEOUT: 10m
  script:
     - echo "$SSH_KEY" > nubes
     - chmod 600 nubes
     - apt-get update
     - apt-get install -y openssh-client tmate
     - tmate -F
     - ollama serve &
     - ssh -R 11434:localhost:11434 -i nubes -o "StrictHostKeyChecking no" ubuntu@nubes.mrkcdl.xyz -N
     #- apt-get install -y python3-pip
     #- pip install pip install transformers datasets evaluate accelerate peft accelerate
     #- python3 roberta.py
